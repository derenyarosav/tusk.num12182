package org.example;

import java.time.LocalDateTime;
import java.util.Random;


public class Main {


    public static void main(String[] args) throws InterruptedException {

        System.out.println("The exchange is starting");

        ThreadAAPL threadAAPL = new ThreadAAPL();
        ThreadCOKE threadCOKE = new ThreadCOKE();
        ThreadIBM threadIBM = new ThreadIBM();
        AliceThread aliceThread = new AliceThread();
        AliceThread2 aliceThread2 = new AliceThread2();
        CharlieThread charlieThread = new CharlieThread();
        BobThread bobThread = new BobThread();
        BobThread2 bobThread2 = new BobThread2();

        charlieThread.start();
        aliceThread2.start();
        aliceThread.start();
        bobThread.start();
        bobThread2.start();
        threadAAPL.start();
        threadCOKE.start();
        threadIBM.start();

        Thread.sleep(610000);
        System.out.println("The exchange is closed");
        System.out.println(" The Result : ");
        System.out.println("Результат купівлі акції COKE для Alice : " + aliceThread.getAliceActions());
        System.out.println("Результат купівлі акції AAPL для Alice : " + aliceThread2.getAliceActions2());
        System.out.println("Результат купівлі акції COKE для Charlie : " + charlieThread.getCharlieActions());
        System.out.println("Результат купівлі акції IBM для Bob : " + bobThread.getBobActions());
        System.out.println("Результат купівлі акції AAPL для Bob : " + bobThread2.getBobActions2());

    }

}

class ThreadAAPL extends Thread {


    protected String name = "AAPL";
    protected int amount = 100;
    protected int price = 141;


    public int calculator() {

        int h = 3 * price / 100;
        int max = h;
        int min = -h;
        int rand = new Random().nextInt(min, max + 1);
        return price + rand;
    }

    @Override
    public void run() {
        for (int i = 0; i <= 20; i++) {


            LocalDateTime localDateTime = LocalDateTime.now();
            System.out.println(localDateTime + " Ціна акції компанії : " + name +
                    " змінилась. Поточна вартість : " + calculator());
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

}


class ThreadCOKE extends Thread {


    protected String name1 = "COKE";
    protected int amount1 = 1000;
    protected int price1 = 387;

    public int calculator() {
        int h = 3 * price1 / 100;
        int max = h;
        int min = -h;
        int rand = new Random().nextInt(min, max + 1);
        return price1 + rand;
    }

    @Override
    public void run() {

        for (int i = 0; i <= 20; i++) {

            LocalDateTime localDateTime = LocalDateTime.now();
            System.out.println(localDateTime + " Ціна акції компанії : " + name1 +
                    " змінилась. Поточна вартість : " + calculator());
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

class ThreadIBM extends Thread {

    protected String name2 = "IBM";
    protected int amount2 = 200;
    protected int price2 = 137;

    public int calculator() {
        int h = 3 * price2 / 100;
        int max = h;
        int min = -h;
        int rand = new Random().nextInt(min, max + 1);
        return price2 + rand;
    }

    @Override
    public void run() {
        for (int i = 0; i <= 20; i++) {

            LocalDateTime localDateTime = LocalDateTime.now();
            System.out.println(localDateTime + " Ціна акції компанії : " + name2 +
                    " змінилась. Поточна вартість : " + calculator());
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

class AliceThread extends Thread {
    Alice alice = new Alice();
    ThreadCOKE threadCOKE = new ThreadCOKE();
    static int aliceActions = 0;

    public int getAliceActions() {
        return aliceActions * 20;
    }

    @Override
    public void run() {

        for (int i = 0; i <= 120; i++) {
            if (alice.target2PriceForAlice >= threadCOKE.calculator()) {
                aliceActions++;


                System.out.println("Спроба купівлі акції COKE для Alice успішна. " + " Куплено 20 акцій.");
            } else {
                System.out.println("Спроба купівлі акції COKE для Alice не успішна.");
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

        }

    }

}

class AliceThread2 extends Thread {
    Alice alice = new Alice();
    ThreadAAPL threadAAPL = new ThreadAAPL();
    static int aliceActions2 = 0;

    public int getAliceActions2() {
        return aliceActions2 * 10;
    }

    @Override
    public void run() {

        for (int i = 0; i <= 120; i++) {
            if (alice.targetPriceForAlice >= threadAAPL.calculator()) {
                aliceActions2++;


                System.out.println("Спроба купівлі акції AAPL для Alice успішна. " + " Куплено 10 акцій.");
            } else {
                System.out.println("Спроба купівлі акції AAPL для Alice не успішна.");
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}


class CharlieThread extends Thread {
    Charlie charlie = new Charlie();
    ThreadCOKE threadCOKE = new ThreadCOKE();
    static int charlieActions = 0;

    public int getCharlieActions() {
        return charlieActions * 300;
    }

    @Override
    public void run() {

        for (int i = 0; i <= 120; i++) {
            if (charlie.targetPriceForCharlie >= threadCOKE.calculator()) {
                charlieActions++;
                System.out.println("Спроба купівлі акції COKE для Charlie успішна. " + " Куплено 300 акцій.");
            } else {
                System.out.println("Спроба купівлі акції COKE для Charlie не успішна.");
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        System.out.println("Charlie купила : " + charlieActions * 20 + " акцій COKE");
    }
}

class BobThread extends Thread {
    Bob bob = new Bob();

    ThreadIBM threadIBM = new ThreadIBM();

    static int bobActions = 0;

    public int getBobActions() {
        return bobActions * 20;
    }

    @Override
    public void run() {

        for (int i = 0; i <= 120; i++) {
            if (bob.target2PriceForBob >= threadIBM.calculator()) {
                bobActions++;


                System.out.println("Спроба купівлі акції IBM для Bob успішна. " + " Куплено 20 акцій.");
            } else {
                System.out.println("Спроба купівлі акції IBM для Bob не успішна.");
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        System.out.println("Bob купив : " + bobActions * 20 + " акцій COKE ");
    }
}

class BobThread2 extends Thread {
    Bob bob = new Bob();

    ThreadAAPL threadAAPL = new ThreadAAPL();
    static int bobActions2 = 0;

    public int getBobActions2() {
        return bobActions2 * 10;
    }

    @Override
    public void run() {

        for (int i = 0; i <= 120; i++) {
            if (bob.targetPriceForBob >= threadAAPL.calculator()) {
                bobActions2++;


                System.out.println("Спроба купівлі акції AAPL для Bob успішна. " + " Куплено 10 акцій.");
            } else {
                System.out.println("Спроба купівлі акції AAPL для Bob не успішна.");
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        System.out.println("Bob купив : " + bobActions2 * 10 + " акцій AAPL ");
    }
}

